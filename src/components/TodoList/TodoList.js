import React from 'react';
import './TodoList.css'
import classnames from 'classnames'

export default class TodoList extends React.Component {
  state = {
    todoList: [
      {
        id: 1,
        text: 'Пойти в жопу',
        done: false
      },
      {
        id: 2,
        text: 'Пойти в магаз',
        done: false
      },
      {
        id: 3,
        text: 'Пойти в магаз еще',
        done: false
      },
      {
        id: 4,
        text: 'Помасштабировать',
        done: false
      },
      {
        id: 5,
        text: 'Поплакать',
        done: false
      },
    ],
    input: ''
  }

  inputChangeHandler = e => {
    this.setState({
      input: e.target.value
    })
  }

  addItemHandler = () => {
    const itemText = this.state.input
    const itemId = this.state.todoList.length + 1
    const newItem = {
      id: itemId,
      text: itemText,
      done: false,
      hide: false
    }
    const newList = this.state.todoList
    newList.push(newItem)
    this.setState({
      todoList: newList
    }, () => console.log(this.state.todoList))
  }

  deleteItem = e => {
    e.stopPropagation()
    const newList = this.state.todoList.map(el => el)
    newList[+e.target.dataset.del - 1].hide = true
    console.log(+e.target.dataset.del - 1)
    this.setState({
      todoList: newList
    }, this.delItemFromState(+e.target.dataset.del - 1))


  }

  changeStatus = e => {
    const arr = this.state.todoList
    arr[e.target.dataset.state - 1].done = !arr[e.target.dataset.state - 1].done
    this.setState({
      todoList: arr
    })
  }

  delItemFromState = (num) => {
    let newList = this.state.todoList.map(el => el)
    newList.splice(num, 1)
    const newNewList = newList.map((el, index) => {
      return {
        id: index + 1,
        text: el.text,
        done: el.done
      }
    })
    setTimeout(() => {
      this.setState({
        todoList: newNewList
      }, () => console.log(this.state.todoList))

    }, 500);

  }

  render () {
    const { todoList, input } = this.state
    return (
      <div className='todo_list'>
        <div className="list-wrapper">
          {
            todoList.map((el, index) => {
              return <div data-state={el.id} onClick={this.changeStatus} className={
                classnames(
                  `todo-item todo-item_${el.id}`, {
                    'todo-item_done' : el.done === true,
                    'todo-item_hide': el.hide === true
                  }
                )
                } key={el.id}>

                {el.text}<div className='delete-btn' data-del={el.id} onClick={this.deleteItem}/></div>
            })
          }
        </div>
        <div className="input-wrapper">
          <input type="text" value={input} onChange={this.inputChangeHandler}/>
          <button className="add-item" onClick={this.addItemHandler}>Добавить</button>
        </div>
      </div>
    )
  }
}