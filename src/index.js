import * as serviceWorker from './serviceWorker';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TodoList from './components/TodoList/TodoList'

ReactDOM.render(<div className='container'>
  <TodoList/>
</div>

  , document.getElementById('root'));

serviceWorker.unregister();
